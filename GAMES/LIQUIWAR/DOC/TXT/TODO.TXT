Liquid War (v5.6.3) - To do




Bug-fixing
==========


  In its latest releases Liquid War is quite stable IMHO. However there are
  still some issues with network under Windows for instance. I'm aware of these
  bugs and I'm trying to fix them but this does really take time.

  I always welcome bug-reports and patches, as making Liquid War W 5.x.x as
  stable and bug-free as possible is really important to me - and most of the
  time players also appreciate stable programs 8-)



Artwork
=======


  It's hard to find people to do that kind of thing. Artists are indeed pretty
  rare, at least artists who wish to create stuff freely... So if you feel like
  adding some theme support to Liquid War, this could *really* help. I used the
  textures I had but it would be nice if one could have a "space" ambiance, an
  "ocean" ambiance or a "desert" ambiance. This could really make the game
  better I think.

  Musics (in midi format) are also appreciated. Tim Chadburn spontaneously
  contributed the first midi files, and it really pleased me 8-)



New features
============


  I regularly receive requests for new features in Liquid War. Of course I do
  not have enough time to implement them all, so they land in my "todo" list.
  However, most "major" evolutions are now planned for Liquid War 6, since the
  code in Liquid War 5 is bloated as hell. It's all right to debug it and code
  minor evolutions, but that's all.

  However, here's a list of what "could be" in the next Liquid War 5.x.x
  releases, although it might never be implemented there and come later with
  Liquid War 6:

  * Network enhancements. Network in LW5 is somewhat buggy and hard to use,
    improvements wouldn't harm.

  * Theme support. This would be a way to get rid of the ugly current fonts and
    menus.



Liquid War 6
============


  Since summer 2005, Liquid War 6, a complete rewrite of Liquid War, is on its
  way. See http://www.ufoot.org/liquidwar/liquidwar6 for more informations.

